#!/bin/bash

# gui way:
# download tmpdisk gui app from: https://github.com/imothee/tmpdisk
# create new tmpdisk, with:
#  - Disk Name = "RamDisk",
#  - Disk Size = ±10% RAM, 
#  - Folders = "Caches/askpermissiond,Caches/BraveSoftware,Caches/com.apple.akd,Caches/com.apple.AMPLibraryAgent,Caches/com.apple.AppleMediaServices,Caches/com.apple.appstore,Caches/com.apple.appstoreagent,Caches/com.apple.cache_delete,Caches/com.apple.commerce,Caches/com.apple.helpd,Caches/com.apple.iBooksX,Caches/com.apple.icloud.fmfd,Caches/com.apple.iCloudHelper,Caches/com.apple.imfoundation.IMRemoteURLConnectionAgent,Caches/com.apple.InstallAssistant.macOS1016,Caches/com.apple.installer,Caches/com.apple.installer.osinstallersetupd,Caches/com.apple.nbagent,Caches/com.apple.nsurlsessiond,Caches/com.apple.parsecd,Caches/com.apple.passd,Caches/com.apple.proactive.eventtracker,Caches/com.apple.remindd,Caches/com.apple.Spotlight,Caches/com.apple.touristd,Caches/com.apple.VideoConference,Caches/com.apple.XprotectFramework.AnalysisService,Caches/com.brave.Browser,Caches/com.crashlytics.data,Caches/com.dustinrue.ControlPlane,Caches/com.flixtools.flixtools,Caches/com.Google.GoogleDrive,Caches/com.google.GoogleEarthPro,Caches/com.google.Keystone,Caches/com.google.SoftwareUpdate,Caches/com.intel.PowerGadget,Caches/com.MattRajca.RetinaCapture,Caches/com.microsoft.autoupdate.fba,Caches/com.microsoft.autoupdate2,Caches/com.microsoft.edgemac,Caches/com.microsoft.teams,Caches/com.operasoftware.Installer.Opera,Caches/com.operasoftware.Opera,Caches/com.panic.Transmit,Caches/com.plausiblelabs.crashreporter.data,Caches/com.runningwithcrayons.Alfred,Caches/com.spotify.client,Caches/com.teamviewer.TeamViewer,Caches/FamilyCircle,Caches/familycircled,Caches/CSXS,Caches/GeoServices,Caches/Google,Caches/Google Earth,Caches/knowledge-agent,Caches/ksfetch,Caches/Maps,Caches/Microsoft,Caches/Microsoft Edge,Caches/net.freemacsoft.AppCleaner,Caches/net.pornel.ImageOptim,Caches/node,Caches/PassKit,Caches/TeamViewer,Caches/Transmit",
#  - mark "Use TmpFS..." and "AutoCreate..." options
# On boot, ur Mac'll be asking for password once to spawn a fresh clean RamDisk for a new session.

# automated way:
while [ ! -d /Volumes ]
do
    echo "waiting..."
    sleep 0.5
done
if [ ! -d /Volumes/RamDisk ]; then
    echo "creating ramdisk..."
    sleep 0.5
    diskutil partitionDisk $(hdiutil attach -nomount ram://$((2048*1638))) 1 GPTFormat APFS 'RamDisk' '100%'
    
    user=`whoami`
    from="/Users/$user/Library/Logs"
    to="/Volumes/RamDisk/Logs"
    mkdir -p $to
    sudo rm -rf /private/var/log
    ln -s $to /private/var/log
    sudo rm -rf $from
    ln -s $to $from
    sudo rm -rf /Library/Logs
    sudo ln -s $to /Library/Logs

    user=`whoami`
    from="/Users/$user/Library/Caches"
    to="/Volumes/RamDisk/Caches"
    declare -a arr=(
     "askpermissiond"
     "BraveSoftware"
     "com.apple.akd"
     "com.apple.AMPLibraryAgent"
     "com.apple.AppleMediaServices"
     "com.apple.appstore"
     "com.apple.appstoreagent"
     "com.apple.cache_delete"
     "com.apple.commerce"
     "com.apple.helpd"
     "com.apple.iBooksX"
     "com.apple.icloud.fmfd"
     "com.apple.iCloudHelper"
     "com.apple.imfoundation.IMRemoteURLConnectionAgent"
     "com.apple.InstallAssistant.macOS1016"
     "com.apple.installer"
     "com.apple.installer.osinstallersetupd"
     "com.apple.nbagent"
     "com.apple.nsurlsessiond"
     "com.apple.parsecd"
     "com.apple.passd"
     "com.apple.proactive.eventtracker"
     "com.apple.remindd"
     "com.apple.Spotlight"
     "com.apple.touristd"
     "com.apple.VideoConference"
     "com.apple.XprotectFramework.AnalysisService"
     "com.brave.Browser"
     "com.crashlytics.data"
     "com.dustinrue.ControlPlane"
     "com.flixtools.flixtools"
     "com.Google.GoogleDrive"
     "com.google.GoogleEarthPro"
     "com.google.Keystone"
     "com.google.SoftwareUpdate"
     "com.intel.PowerGadget"
     "com.MattRajca.RetinaCapture"
     "com.microsoft.autoupdate.fba"
     "com.microsoft.autoupdate2"
     "com.microsoft.edgemac"
     "com.microsoft.teams"
     "com.operasoftware.Installer.Opera"
     "com.operasoftware.Opera"
     "com.panic.Transmit"
     "com.plausiblelabs.crashreporter.data"
     "com.runningwithcrayons.Alfred"
     "com.spotify.client"
     "com.teamviewer.TeamViewer"
     "FamilyCircle"
     "familycircled"
     "CSXS"
     "GeoServices"
     "Google"
     "Google Earth"
     "knowledge-agent"
     "ksfetch"
     "Maps"
     "Microsoft"
     "Microsoft Edge"
     "net.freemacsoft.AppCleaner"
     "net.pornel.ImageOptim"
     "node"
     "PassKit"
     "TeamViewer"
     "Transmit"
    )

    for i in "${arr[@]}"
    do
     mkdir -p "$to/$i"
     if [[ -L "$from/$i" && -d "$from/$i" ]]
     then
      echo "$i is already a symlink"
     else
      rm -rf "$from/$i"
      ln -s "$to/$i" $from
     echo "$i CREATED"
     fi
    done
fi

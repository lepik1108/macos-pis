# Homebrew installation:
# /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Install Yabai (modern tiling for macOS 11+), or Amethyst for older versions
macos=`sw_vers | grep ProductVersion | awk '{print $2}'`
function v { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }
if [ $(v $macos) -ge $(v "11.0.0") ]; then
  echo "Installing Yabai: https://github.com/koekeishiya/yabai"
  brew install koekeishiya/formulae/yabai
  mkdir -p ~/.config/yabai
  cp `pwd`/yabairc ~/.config/yabai/yabairc
  brew services restart yabai
  # # lines below requires csrutil to be disabled, which is not best solution securitywise :(
  # # anyway, if you trust yourself, follow https://github.com/koekeishiya/yabai/wiki/Installing-yabai-(latest-release)#configure-scripting-addition
  # user=`whoami`
  # yabai=`which yabai`
  # hash=`shasum -a 256 $yabai`
  # sudo echo "$user ALL=(root) NOPASSWD: sha256:$hash $yabai --load-sa" > /private/etc/sudoers.d/yabai
  # mkdir -p ~/.config/yabai
elif [ $(v $macos) -ge $(v "10.15.0") ]; then
  echo "Installing Amethyst: https://ianyh.com/amethyst"
  brew install --cask amethyst
fi

# Usefull tools which'll be innstalled are described here:
# https://medium.com/macoclock/awesome-command-line-tools-for-the-mac-42d810dacf93

brew install fish
brew install bat
brew install mtr
brew install lnav
brew install mycli
brew install pandoc
brew install httpie

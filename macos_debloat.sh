# There are in general many services used by Apple
# So if you find any other useless service that can be removed please leave a comment
# use the following to see all services in play: `launchctl list`
# Some general services that I don't use 
sudo launchctl disable com.apple.CallHistoryPluginHelper
sudo launchctl disable com.apple.AddressBook.abd
sudo launchctl disable com.apple.ap.adprivacyd
sudo launchctl disable com.apple.ReportPanic
sudo launchctl disable com.apple.ReportCrash
sudo launchctl disable com.apple.ReportCrash.Self
sudo launchctl disable com.apple.DiagnosticReportCleanup.plist
sudo launchctl disable com.apple.ap.adprivacyd
sudo launchctl disable com.apple.siriknowledged
sudo launchctl disable com.apple.helpd
sudo launchctl disable com.apple.mobiledeviceupdater
sudo launchctl disable com.apple.TrustEvaluationAgent
sudo launchctl disable com.apple.iTunesHelper.launcher
sudo launchctl disable com.apple.softwareupdate_notify_agent
sudo launchctl disable com.apple.familycircled
#sudo launchctl disable com.apple.appstoreagent
#sudo launchctl disable com.apple.screensharing.MessagesAgent
sudo launchctl remove com.apple.CallHistoryPluginHelper
sudo launchctl remove com.apple.AddressBook.abd
sudo launchctl remove com.apple.ap.adprivacyd
sudo launchctl remove com.apple.ReportPanic
sudo launchctl remove com.apple.ReportCrash
sudo launchctl remove com.apple.ReportCrash.Self
sudo launchctl remove com.apple.DiagnosticReportCleanup.plist
sudo launchctl remove com.apple.ap.adprivacyd
sudo launchctl remove com.apple.siriknowledged
sudo launchctl remove com.apple.helpd
sudo launchctl remove com.apple.mobiledeviceupdater
sudo launchctl remove com.apple.TrustEvaluationAgent
sudo launchctl remove com.apple.iTunesHelper.launcher
sudo launchctl remove com.apple.softwareupdate_notify_agent
sudo launchctl remove com.apple.familycircled
#sudo launchctl remove com.apple.appstoreagent
#sudo launchctl remove com.apple.screensharing.MessagesAgent

# disable Mac's antimalware service
sudo launchctl disable gui/501/com.apple.MRTa
sudo launchctl remove gui/501/com.apple.MRTa

# Don't use Safari? Uncomment lines below:
#sudo launchctl remove com.apple.SafariCloudHistoryPushAgent
#sudo launchctl remove com.apple.Safari.SafeBrowsing.Service
#sudo launchctl remove com.apple.SafariNotificationAgent
#sudo launchctl remove com.apple.SafariPlugInUpdateNotifier
#sudo launchctl remove com.apple.SafariHistoryServiceAgent
#sudo launchctl remove com.apple.SafariLaunchAgent
#sudo launchctl remove com.apple.SafariPlugInUpdateNotifier
#sudo launchctl remove com.apple.safaridavclient

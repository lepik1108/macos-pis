# MacOS-PIS
MacOS Post Install Scripts

## Usage
```
git clone https://gitlab.com/lepik1108/macos-pis.git
cd macos-pis
chmod +x ./*.sh
```
To disable annoying apple services and setup a RAM disk for most common applications cache, just run:
```
./macos_debloat.sh
./macos_ramdisk.sh
cp `pwd`/com.user.ramdisk.plist /Library/LaunchAgents/com.user.ramdisk.plist
chmod +x /Library/LaunchAgents/com.user.ramdisk.plist

```
To install usefull tools like modern terminal(fish), tiling manager(yabai)... you'll just require homebrew, so if it's not installed:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
IF Homebrew is already installed, just:
```
./usefull_tools.sh
```
## Files short description

#### macos_debloat.sh
Disables/removes unused apple services

#### macos_ramdisk.sh
Creates a RAM disk with size 1.6Gb(enough for session cashes and logs)

#### com.user.ramdisk.plist
Just copy this file to startup folder:
```
# add service to auto startup at login
cp `pwd`/com.user.ramdisk.plist /Library/LaunchAgents/com.user.ramdisk.plist
chmod +x /Library/LaunchAgents/com.user.ramdisk.plist
```
#### usefull tools
Innstalls Homebrew and few awesome console tools

#### yabairc
It's a simple connfig file of Yabai, provides basic tiling functionality without keybinds
To apply your custom yabai config, just edit `~/.config/yabai/yabairc` and run
```
  brew services restart yabai
```
